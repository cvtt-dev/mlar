<section class="ml-sections ml-sections__slides">

    <div class="ml-slides">
        <?php $slides = new WP_Query(array('post_type' => 'slides', 'showposts' => -1, 'orderby' => 'date'));

        while ($slides->have_posts()) : $slides->the_post(); ?>
            <div class="banner banner-rsppnsv">

                <?php
                $imgMobile   = get_post_meta(get_the_ID(), 'slideImgMobile', true);
                $imgDesktop   = get_post_meta(get_the_ID(), 'slideImgDesktop', true);
                $slideLink   = get_post_meta(get_the_ID(), 'slide_link', true);
                $slideTarget   = get_post_meta(get_the_ID(), 'slide_target', true);
                $slideTitle  = get_post_meta(get_the_ID(), 'slide_btn', true);




                echo ($slideLink && $slideTarget) ? '<a class="link-slide" href="' . $slideLink . '" target="' . $slideTarget . '" title="' . $slideTitle . '" rel="noopener noreferrer"></a>' : '';
                
                
                ?>

                <img class="banner-dsktp" src="<?php echo wp_get_attachment_image_url($imgDesktop, 'full') ?>">
                <img class="banner-mbl" src="<?php echo wp_get_attachment_image_url($imgMobile, 'full') ?>">
            </div>

        <?php endwhile;
        wp_reset_query(); ?>
    </div>

</section>