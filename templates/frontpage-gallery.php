<?php
$settings = get_option('options_gerais');
$imgs     = $settings['galeria_images'];
?>


<div id="galeria" class="section-8 section-galeria galeria">
    <div class="main-container-6">
        <div class="heading-centered _100">
            <h1 class="body-heading">Galeria de Fotos<br></h1>
            <div class="large-text">Conheça cada detalhe do M. Lar Jacarey</div>
        </div>
    </div>
    <div class="gallery_slide-wrap handwriting-wrapper">
        <div class="btn-nav-gallery first-slide-is-active">
            <button class="arrow-left"></button>
            <button class="arrow-right"></button>
        </div>
        <div class="gallery_slide ml-slid-fc-dn">
            <?php $i = 0;
            foreach ($imgs as $img) : ?>
                <figure class="figure-item">
                    <a class="link-item" href="<?php echo wp_get_attachment_url($img); ?>" data-caption="<?php echo wp_get_attachment_caption($img); ?>">
                        <img class="image-item" src="<?php echo wp_get_attachment_url($img); ?>" alt="">

                    </a>



                    <p class="caption">
                        <?php echo wp_get_attachment_caption($img); ?>
                    </p>
                </figure>

            <?php $i++;
            endforeach; ?>

        </div>
        <!-- <div class="div-block-4 mb">
                <a href="#form" class="button-2 small outline-white center bt w-inline-block">
                    <div class="text-block-21 whtas-txt-2 whts-big">Viva o bem estar</div>
                </a>
            </div> -->
    </div>

</div>