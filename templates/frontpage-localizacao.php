<div id="localizacao" class="section-6 nopadding maps-padding">

  <div class="container-4 container-loca">
    <div class="heading-centered _100">
      <h1 class="body-heading-3 tit_sessao titsolucoes"><strong class="bold-text-7">Localização</strong><br></h1>
      <div class="large-text">Veja a localização do M.Lar no lago Jacarey</div>
    </div>
    <div class="loc-wra-btn">

      <a href="https://www.google.com/maps/place/Comunidade+da+F%C3%A9+Crist%C3%A3+Zona+Sul/@-3.8057756,-38.4882221,18.5z/data=!4m5!3m4!1s0x7c74f81325727c7:0x90db71903ca89a3c!8m2!3d-3.8055269!4d-38.4884967?hl=pt-BR" target="_blank" class="button-2 small outline-white center bt w-inline-block">
        <div class="text-block-21 whtas-txt-2 whts-big">Como Chegar</div>
      </a>
      <a href="#contato" target="_self" class="button-2 small outline-white center bt w-inline-block">
        <div class="text-block-21 whtas-txt-2 whts-big">Agendar</div>
      </a>
    </div>
  </div>

  <div class="main-container-7 _100"></div>
  <div class="w-embed w-iframe">    
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1990.509951778038!2d-38.4882221!3d-3.8057756!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74f81325727c7%3A0x90db71903ca89a3c!2sComunidade%20da%20F%C3%A9%20Crist%C3%A3%20Zona%20Sul!5e0!3m2!1spt-BR!2sbr!4v1629310704534!5m2!1spt-BR!2sbr" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
</div>