<div id="plantas" class="section-7 border-top ml-plantas">
  <div class="container-6">
    <div class="w-layout-grid grid-halves-2">
      <div class="container-small-2 align-center">
        <div class="section-contents">
          <div class="content-width-small">
            <div class="hero-sub-heading sublack sub_titulo menor branco">Plantas<br></div>
            <!-- <h1 class="body-heading-2 titulowhite titulo-3"><span class="text-span-9">O seu futuro lar </br>é do Jeito que você merece.</span><br></h1> -->
          </div>
          <!-- <div class="hero-sub-heading sublack sub_titulo m0 menor branco">Apartamentos de 48m2 e 58m2, fino acabamento e estrutura completa<br></div> -->
          <div id="atributPlanta" class="badge-group">

          </div>
          <a href="#form" class="button-2 small outline-white center bt w-inline-block">
            <div class="text-block-21 whtas-txt-2 whts-big">Saiba mais</div>
          </a>
        </div>
      </div>
      <div class="container-large content-width-medium wrap-planta">



        <div class="plantas-slick">
          <a class="item" href="<?php echo get_template_directory_uri() ?>/uploads/images/1_1.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/uploads/images/1_1.jpg">
            <ul class="dados-list" style="display:none;">
              <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
                <div class="text-block-30">planta 1 48m2 e 58m2<br></div>
              </div>
              <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
                <div class="text-block-32">planta 1 2 ou 3 quartos sendo 1 suíte</div>
              </div>
              <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
                <div class="text-block-31">planta 1 1 ou 2 vagas</div>
              </div>
              <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
                <div class="text-block-34">planta 1 Área de lazer completa</div>
              </div>
              <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
                <div class="text-block-33">planta 1 Ótima localização</div>
              </div>
            </ul>
          </a>


          <a class="item" href="<?php echo get_template_directory_uri() ?>/uploads/images/2_1.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/uploads/images/2_1.jpg">
            <ul class="dados-list" style="display:none;">
             <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-30">planta 2 48m2 e 58m2<br></div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-32">planta 2 2 ou 3 quartos sendo 1 suíte</div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-31">planta 2 1 ou 2 vagas</div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-34">planta 2 Área de lazer completa</div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-33">planta 2 Ótima localização</div>
            </div>
            </ul>
          </a>
          <a class="item" href="<?php echo get_template_directory_uri() ?>/uploads/images/3_1.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/uploads/images/3_1.jpg">
            <ul class="dados-list" style="display:none;">
             <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-30">planta 3 48m2 e 58m2<br></div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-32">planta 3 2 ou 3 quartos sendo 1 suíte</div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-31">planta 3 1 ou 2 vagas</div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-34">planta 3 Área de lazer completa</div>
            </div>
            <div class="badge-2 badge-group-item bg-white"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/check.png" alt="" class="badge-icon">
              <div class="text-block-33">planta 3 Ótima localização</div>
            </div>
            </ul>
          </a>

        </div>


        <div class="btn-nav-plantas">
          <div class="button-5 icon-button overlay-slider-button-left w-slider-arrow-left"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/icon-arrow-left.svg" alt="" class="button-icon-image"></div>
          <div class="button-5 icon-button overlay-slider-button-right w-slider-arrow-right"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/icon-arrow-right.svg" alt="" class="button-icon-image"></div>
        </div>

      </div>
    </div>
  </div>
</div>