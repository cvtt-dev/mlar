<div id="login-modal" style="display: none;" class="form-car__wrap">
    <div class="form-wrp">
    
            <h3 class="title-form-cliente">Área do cliente</h3>
            <form id="area_cliente" role="form" class="mq-modal-cliente" method="POST" onsubmit="return false;">
                <div class="login_msg"></div>
                <div class="mq-modal-cliente__form-group">
                    <input type="text" class="form-control" id="login" name="login" placeholder="CPF ou Código de Cliente">
                </div>
                <div class="mq-modal-cliente__form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Senha">
                </div>
                <button type="submit" class="mq-modal-cliente__btn btn">Entrar <i id="cliente-login-load" class="fa fa-cog fa-spin hidden"></i></button>
            </form>
            <a href="#" class="mq-modal-cliente__reset-password" id="reset-password">Esqueci minha senha</a>
            <form id="form-reset-password" role="form" action="" class="mq-modal-cliente mq-modal-cliente--forget-password" method="POST" onsubmit="return true;">
                <div class="login_msg"></div>
                <div class="mq-modal-cliente__form-group mq-modal-cliente__form-group-pass">
                    <input type="text" class="form-control" id="email_reset" name="email" placeholder="Digite seu e-mail">
                </div>
                <h3 class="mq-modal-cliente__title-pass">Digite seu e-mail para receber informações sobre o login</h3>
                <button type="submit" class="mq-modal-cliente__btn btn" id="reset">Enviar <i id="cliente-login-load" class="fa fa-cog fa-spin hidden"></i></button>
            </form>
     
    </div>
</div>