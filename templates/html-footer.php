<div class="footer-2">
  <div class="main-container-9">
    <div class="w-layout-grid footer-2-top-row">
      <div>
        <div class="container-4 container-loca">
          <div class="heading-centered _100">
            <div class="large-text txt-footer">Viva em uma região que sempre sonhou.<br><span class="text-span-15">Agende sua visita hoje mesmo</span></div>
          </div>
          <div class="div-block-5"></div>
          <a href="#" class="button-2 small outline-white center bt bt-visita w-inline-block">
            <div class="text-block-21 whtas-txt-2 whts-big txt-bt-visita">agendar sua visita</div>
          </a>
          <div class="large-text txt-endereco">Rua Amâncio Valente, 830 – Cambeba | CEP: 60822-155, Fortaleza/CE<br>08:00 AM - 6:30 PM</div><img src="<?php echo get_template_directory_uri() ?>/uploads/images/Retângulo-19_1Retângulo-19.png" loading="lazy" alt="" class="image-121 iconparalax">
        </div>
      </div>
    </div>
  </div>
  <div class="horizontal-rule"></div>
  <div class="main-container-9">
    <div class="footer-2-bottom-row">
      <!-- <a href="#" class="footer-2-logo w-inline-block"><img src="<?php// echo get_template_directory_uri() ?>/uploads/images/logo-marquise.png" alt=""></a> -->
      <div class="footer-menu-wrap-item">

        <div class="footer-meta">

          <div class="footer-meta-item align-center">
            <a href="mailto:contato@mlar.com.br" target="_blank" class="underline-link text-white w-inline-block">
              <div class="small-text">Contato</div>
              <div class="underline-container bg-white">
                <div class="underline-line bg-white"></div>
              </div>
            </a>
          </div>
          <div class="footer-meta-item align-center">
            <a href="mailto:recrutamento@mlar.com.br" target="_blank" class="underline-link text-white w-inline-block">
              <div class="small-text">Trabalhe conosco</div>
              <div class="underline-container bg-white">
                <div class="underline-line bg-white"></div>
              </div>
            </a>
          </div>
          <div class="footer-meta-item align-center">
            <a href="http://www.grupomarquise.com.br/" target="_blank" class="underline-link text-white w-inline-block">
              <div class="small-text">Uma empresa do Grupo Marquise</div>
              <div class="underline-container bg-white">
                <div class="underline-line bg-white"></div>
              </div>
            </a>
          </div>

        </div>



      </div>
      <div class="social-links footer-social-links">
        <a href="https://www.instagram.com/mlarempreendimentos/" target="_blank" class="social-link-item w-inline-block"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/instagram.svg" alt="" class="social-link-image"></a>
        <a href="https://www.facebook.com/mlarempreendimentos" target="_blank" class="social-link-item w-inline-block"><img src="https://uploads-ssl.webflow.com/5f5a3c1606e4e2798a5eed6f/5f5a3c177f7305eff8cb9092_facebook.svg" alt="" class="social-icon"></a>
        <a href="https://www.youtube.com/channel/UCkX4iQwzcYNsaAhp7okDlCg" target="_blank" class="social-link-item w-inline-block"><img src="<?php echo get_template_directory_uri() ?>/assets/images/youtube.svg" alt="" class="social-icon"></a>
      </div>
      <div class="wrapper-2">
        <div class="bottom">
          <div class="small-text-2">© Todos os direitos reservados<br></div>
          <a href="https://convertte.com.br" target="_blank" class="link-block-7 w-inline-block"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/cvtt.png" loading="lazy" alt=""></a>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
<script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5fda1c780f644012d9821bcb" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/webflow.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.fancybox.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.mask.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/ajax.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
<?php wp_footer(); ?>
</body>

</html>