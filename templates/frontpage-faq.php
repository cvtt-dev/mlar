<?php query_posts(array("posts_per_page" => -1, "post_type" => "faq"));?> 
    <div id="faq" class="section section-cta section-perguntas">
      <div class="container-4 container-loca">
        <div class="heading-centered _100">
          <h1 class="body-heading-3 tit_sessao titsolucoes"><strong class="bold-text-7">Perguntas frequentes</strong><br></h1>
          <!-- <div class="large-text">Veja a localização do M.Lar no lago Jacarey Veja a localização do M.Lar no lago Jacarey</div> -->
        </div>
        <div class="accordion-container">
        <?php while(have_posts()) : the_post(); ?>

          <div class="accordion-item">
            <div class="accordion-item-title">
              <h6 class="accordion-heading"><?php the_title();?></h6><img src="<?php echo get_template_directory_uri() ?>/uploads/images/icon-chevron-down.svg" alt="" class="accordion-arrow-2">
            </div>
            <div class="accordion-item-content-wrapper">
              <div class="accordion-item-content">
                <div class="text-block-36"><?php the_content();?></div>
              </div>
            </div>
          </div>


          <?php endwhile; wp_reset_query();?>

          
        </div>
      </div>
    </div>