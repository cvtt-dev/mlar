<?php

function get_assets()
{
    wp_enqueue_style('css', get_template_directory_uri() . '/_informacon/restrito.css', array(), null);

}

add_action('wp_enqueue_scripts', 'get_assets', 100);

function marquiseGetXml($url)
{
    // $ch = curl_init();

    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

    // $xmlstr = curl_exec($ch);
    // curl_close($ch);

    // return $xmlstr;



    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Access-Control-Allow-Origin: *',
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    return  $response;
    



}

function informacon_get_user()
{
    session_start();

    $login  = $_POST['login'];
    $senha  = $_POST['senha'];
    $url    = "http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/validausuario?CLILOG=" . $login . "&CLISEN=" . $senha;

    if (!$login || !$senha) {
        $status = 2;
    } else {
        $xmlstr = marquiseGetXml($url);
        $xmlobj = new SimpleXMLElement($xmlstr);
        if ($xmlobj->validacaoresultado == 'OK' && $xmlobj->listacontratos->contrato_identificador_item->empreendimento_codigo == "JAC.01.0001") {
            $_SESSION['login'] = $login;
            $_SESSION['senha'] = $senha;
            $_SESSION['xml']   = $xmlobj->asXML();
            $status = 1;
        } else {
            $status = 2;
        }
    }

    echo json_encode($status);
    die();
}

add_action('wp_ajax_getUser', 'informacon_get_user');
add_action('wp_ajax_nopriv_getUser', 'informacon_get_user');
