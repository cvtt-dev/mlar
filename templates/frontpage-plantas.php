<?php
$settings = get_option('options_gerais');
$imgs     = $settings['plantas_images'];
?>

<div id="plantas" class="section-7 border-top ml-plantas">
  <!-- <div class="container-6">
    <div class="w-layout-grid grid-halves-2">
      <div class="container-small-2 align-center">
        <div class="section-contents">
          <div class="content-width-small">
            <div class="hero-sub-heading sublack sub_titulo menor branco">Plantas<br></div>
    <h1 class="body-heading-2 titulowhite titulo-3"><span class="text-span-9">O seu futuro lar </br>é do Jeito que você merece.</span><br></h1> 
          </div>
        <div class="hero-sub-heading sublack sub_titulo m0 menor branco">Apartamentos de 48m2 e 58m2, fino acabamento e estrutura completa<br></div>
          <div id="atributPlanta" class="badge-group">

          </div>
        
        </div>
      </div>
    </div>

    </div> -->



    <div class="main-container-6">
        <div class="heading-centered _100">
            <h1 class="body-heading">Plantas<br></h1>
            <!-- <div class="large-text">conheça cada detalhe da sua
                nova vida</div> -->
        </div>
    </div>


    <div class="plantas_slide-wrap handwriting-wrapper">
      <div class="btn-nav-plantas first-slide-is-active">
        <button class="arrow-left"></button>
        <button class="arrow-right"></button>
      </div>
      <div class="plantas_slide ml-slid-fc-dn">
        <?php $i = 0;
        foreach ($imgs as $img) : ?>
          <figure class="figure-item <?php echo ($i > 8) ? "none" : "visible"; ?>">
            <a class="link-item" href="<?php echo wp_get_attachment_url($img); ?>" data-caption="<?php echo wp_get_attachment_caption($img); ?>">
              <img class="image-item" src="<?php echo wp_get_attachment_url($img); ?>" alt="">

            </a>



            <p class="caption">
              <?php echo wp_get_attachment_caption($img); ?>
            </p>
          </figure>

        <?php $i++;
        endforeach; ?>

      </div>
      <!-- <div class="div-block-4 mb">
                <a href="#form" class="button-2 small outline-white center bt w-inline-block">
                    <div class="text-block-21 whtas-txt-2 whts-big">Viva o bem estar</div>
                </a>
            </div> -->
    </div>


</div>