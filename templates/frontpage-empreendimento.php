<div id="empreendimento" class="section bg-gray-4 novoform pb-80">
      <div class="container-9">
        <div class="row row-split-content">
          <div class="content-width-medium">
            <div class="card no-border shadow-small"><img src="<?php echo get_site_url() ?>/wp-content/uploads/2021/05/fachada-mlar.png" loading="lazy" sizes="(max-width: 479px) 95vw, (max-width: 767px) 66vw, (max-width: 991px) 67vw, (max-width: 1919px) 40vw, 465.421875px" srcset="<?php echo get_site_url() ?>/wp-content/uploads/2021/05/fachada-mlar.png 500w, <?php echo get_site_url() ?>/wp-content/uploads/2021/05/fachada-mlar.png 800w, <?php echo get_template_directory_uri() ?>/wp-content/uploads/2021/05/fachada-mlar.png 1080w, <?php echo get_site_url() ?>/wp-content/uploads/2021/05/fachada-mlar.png 1130w" alt="" class="image-119"></div>
          </div>
          <div class="content-width-small">
            <div class="hero-sub-heading sublack sub_titulo menor">SOBRE O EMPREENDIMENTO<br>
          </div>
          <h1 class="body-heading-2 titulowhite titulo-3"><span class="text-span-9">CONHEÇA O M.LAR JACAREY</span><br></h1>
			    <div class="large-text mb-30">Um projeto criado com muito carinho, pensado em cada detalhe para você. A localização é incrível: A 2 minutos do Lago Jacarey, com tudo que você sempre sonhou. Apartamentos de 2 ou 3 quartos e um condomínio com lazer completo.</div>
			  	<div class="large-text mb-30">
            <div class="price-text">A partir de: R$: 240mil*</div>
            <div class="juridico-text">Este valor refere-se à condição de pagamento a vista da unidade 101 com área privativa de 51,11 m² da Torre Soul do empreendimento M.Lar Jacarey. Registrado sob R.I-R-10 da matrícula nº 19.713, do 1º CRI. (Referente à Tabela de Vendas de Agosto/2021).</div>
				  </div>
			 <ul role="list" class="list-grid margin-top w-list-unstyled">
              <li class="icon-grid-list-item">
                <div class="circle-small circle-big"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/area.png" alt=""></div>
                <div class="text-space-left-large-2">
                  <div class="txt-lista"><strong>Área 51,11m²  a 62,87m²</strong></div>
                  
                </div>
              </li>
              <li class="icon-grid-list-item">
                <div class="circle-small circle-big"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/lazer.png" alt=""></div>
                <div class="text-space-left-large-2">
                  <div class="txt-lista"><strong>Área de lazer completa</strong></div>
                  
                </div>
              </li>
              <li class="icon-grid-list-item">
                <div class="circle-small circle-big"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/quartos.png" alt=""></div>
                <div class="text-space-left-large-2">
                  <div class="txt-lista"><strong>2 ou 3 quartos</strong></div>
               
                </div>
              </li>
              <li class="icon-grid-list-item">
                <div class="circle-small circle-big"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/vagas.png" alt=""></div>
                <div class="text-space-left-large-2">
                  <div class="txt-lista"><strong>1 vaga de garagem</strong></div>
                
                </div>
              </li>
            </ul>
			     
            <div class="div-block">
              <div class="content-width-medium-2"></div>
            </div>
            <!-- <a href="#" class="button-2 small outline-white center bt w-inline-block">
              <div class="text-block-21 whtas-txt-2 whts-big">Tour Virtual</div>
            </a> -->
          </div>
        </div>
      </div>
    </div>