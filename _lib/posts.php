<?php
/**
* Custom Post Types
* Desenvolvedor:Alex Freitas
*/



//=========================================================================================
// POST TYPE PLANTAS
//=========================================================================================

function post_type_plantas_register() {
    $labels = array(
        'name' => 'Plantas',
        'singular_name' => 'Planta',
        'menu_name' => 'Plantas',
        'add_new' => _x('Adicionar Planta', 'item'),
        'add_new_item' => __('Adicionar Nova Planta'),
        'edit_item' => __('Editar Planta'),
        'new_item' => __('Nova Planta')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'plantas'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-layout',
        'supports' => array('title')
    );
    register_post_type('plantas', $args);
}
add_action('init', 'post_type_plantas_register');



//=========================================================================================
// POST TYPE SLIDES
//=========================================================================================

function post_type_slide_register() {
    $labels = array(
        'name' => 'Slides',
        'singular_name' => 'Slide',
        'menu_name' => 'Slides',
        'add_new' => _x('Adicionar Slide', 'item'),
        'add_new_item' => __('Adicionar Novo Slide'),
        'edit_item' => __('Editar Slide'),
        'new_item' => __('Novo Slide')
    );
    
    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slides'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt2',
        'supports' => array('title')
    );
    register_post_type('slides', $args);
}
add_action('init', 'post_type_slide_register');




//=========================================================================================
// POST TYPE PERGUNTAS FREQUENTES
//=========================================================================================

function post_type_faq_register() {
    $labels = array(
        'name' => 'FAQ',
        'singular_name' => 'FAQ',
        'menu_name' => 'FAQ',
        'add_new' => _x('Adicionar Pergunta', 'item'),
        'add_new_item' => __('Adicionar Nova Pergunta'),
        'edit_item' => __('Editar Pergunta'),
        'new_item' => __('Nova Pergunta')
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'faq'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-layout',
        'supports' => array('title', 'editor')
    );
    register_post_type('faq', $args);
}
add_action('init', 'post_type_faq_register');




// //=========================================================================================
// // POST TYPE VANTAGENS
// //=========================================================================================

// function post_type_vantagens_register() {
//     $labels = array(
//         'name' => 'Vantagens',
//         'singular_name' => 'Vantagem',
//         'menu_name' => 'Vantagens',
//         'add_new' => _x('Adicionar Vantagem', 'item'),
//         'add_new_item' => __('Adicionar Nova Vantagem'),
//         'edit_item' => __('Editar Vantagem'),
//         'new_item' => __('Nova Vantagem')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => false,
//         'publicly_queryable' => false,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'vantagens'),
//         'capability_type' => 'post',
//         'has_archive' => false,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-images-alt2',
//         'supports' => array('title')
//     );
//     register_post_type('vantagens', $args);
// }
// add_action('init', 'post_type_vantagens_register');



