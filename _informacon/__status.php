<?php
$code = $c->empreendimento_codigo;

$args = array(
    'post_type'     => 'mar_empreendimentos',
    'meta_query' => array(
        array(
            'key' => 'wpcf_cod_empreendimento',
            'value' => like_escape($code),
            'compare' => 'EQUAL'
        )
    ),
    'showposts'     => 1,
);

$empreendimento = get_posts($args);

$planilha       = get_post_meta( $empreendimento[0]->ID, 'emp_planilha', true);
$planilha_sind  = get_post_meta( $empreendimento[0]->ID, 'emp_planilha_sind', true);

// var_dump( $planilha );

if($planilha || $planilha_sind):
    echo '<div class="area-informacoes__wrap-small">';
    echo '<h3 class="title-manual">Manuais do Empreendimento</h3>';
    echo '<div class="area-informacoes__btns-wrap">';

        if($planilha):
        echo '<a href="'.wp_get_attachment_url($planilha).'" download="Manual Proprietário - '.get_the_title($empreendimento[0]->ID).'" target="_blank">Manual do Proprietário</a>';
        endif;
        if($planilha_sind):
            echo '<a href="'.wp_get_attachment_url($planilha_sind).'" download="Manual Síndico - '.get_the_title($empreendimento[0]->ID).'" target="_blank">Manual do Síndico</a>';
        endif;

    echo '</div>';
    echo '</div>';
endif;

/* STATUS*/

$status_geral                   = get_post_meta( $empreendimento[0]->ID, 'wpcf_status_geral', true);
$limpeza_terreno                = get_post_meta( $empreendimento[0]->ID, 'wpcf_limpeza_terreno', true);
$escavacao                      = get_post_meta( $empreendimento[0]->ID, 'wpcf_escavacao', true);
$contencao                      = get_post_meta( $empreendimento[0]->ID, 'wpcf_contencao', true);
$fundacao                       = get_post_meta( $empreendimento[0]->ID, 'wpcf_fundacao', true);
$superestrutura                 = get_post_meta( $empreendimento[0]->ID, 'wpcf_superestrutura', true);
$alvenarias                     = get_post_meta( $empreendimento[0]->ID, 'wpcf_alvenarias', true);
$impermeabilizacao              = get_post_meta( $empreendimento[0]->ID, 'wpcf_impermeabilizacao', true);
$reboco_emboco                  = get_post_meta( $empreendimento[0]->ID, 'wpcf_reboco_emboco', true);
$contrapiso                     = get_post_meta( $empreendimento[0]->ID, 'wpcf_contrapiso', true);
$revestimento_interno           = get_post_meta( $empreendimento[0]->ID, 'wpcf_revestimento_interno', true);
$rejuntamento_interno           = get_post_meta( $empreendimento[0]->ID, 'wpcf_rejuntamento_interno', true);
$divisorias_gesso               = get_post_meta( $empreendimento[0]->ID, 'wpcf_divisorias_gesso', true);
$forro_gesso                    = get_post_meta( $empreendimento[0]->ID, 'wpcf_forro_gesso', true);
$esquadrias_aluminio            = get_post_meta( $empreendimento[0]->ID, 'wpcf_esquadrias_aluminio', true);
$guarda_corpo                   = get_post_meta( $empreendimento[0]->ID, 'wpcf_guarda_corpo', true);
$portas_madeira                 = get_post_meta( $empreendimento[0]->ID, 'wpcf_portas_madeira', true);
$assentamento_bancadas          = get_post_meta( $empreendimento[0]->ID, 'wpcf_assentamento_bancadas', true);
$assentamento_louças_metais     = get_post_meta( $empreendimento[0]->ID, 'wpcf_assentamento_louças_metais', true);
$pintura_prim_demao             = get_post_meta( $empreendimento[0]->ID, 'wpcf_pintura_prim_demao', true);
$pintura_seg_demao              = get_post_meta( $empreendimento[0]->ID, 'wpcf_pintura_seg_demao', true);
$limpeza_final                  = get_post_meta( $empreendimento[0]->ID, 'wpcf_limpeza_final', true);
$fiacao_eletrica                = get_post_meta( $empreendimento[0]->ID, 'wpcf_fiacao_eletrica', true);
$acabamentos_eletricos          = get_post_meta( $empreendimento[0]->ID, 'wpcf_acabamentos_eletricos', true);
$ramais_esgoto_aguas_pluviais   = get_post_meta( $empreendimento[0]->ID, 'wpcf_ramais_esgoto_aguas_pluviais', true);
$ramais_agua_fria_quente        = get_post_meta( $empreendimento[0]->ID, 'wpcf_ramais_agua_fria_quente', true);
$ramais_incendio                = get_post_meta( $empreendimento[0]->ID, 'wpcf_ramais_incendio', true);
$instalacoes_gas                = get_post_meta( $empreendimento[0]->ID, 'wpcf_instalacoes_gas', true);
$instalacoes_ar_condicionado    = get_post_meta( $empreendimento[0]->ID, 'wpcf_instalacoes_ar_condicionado', true);
$revestimento_fachada           = get_post_meta( $empreendimento[0]->ID, 'wpcf_revestimento_fachada', true);
$limpeza_fachada                = get_post_meta( $empreendimento[0]->ID, 'wpcf_limpeza_fachada', true);

if ( $status_geral ) :

    echo '<div id="box-status" class="box-bars">';
        echo '<h5><b>Status da obra</b></h5>';
        // echo '<h2 class="status-tit-pri">Status geral da obra</h2>';

        echo '<div class="status-bar">';
            echo '<div class="bar-progress" data-total="'.$status_geral.'">';
                echo '<div class="percent">'.$status_geral.'<em>%</em></div>';
            echo '</div>';
        echo '</div>';
        echo '<h3 class="status-tit">Status Geral</h3>';

        echo '<ul class="list-status">';

        if($limpeza_terreno) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$limpeza_terreno.'">
                    <div class="percent">'.$limpeza_terreno.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Limpeza do terreno</h3>
            </li>';
        }
        if($escavacao) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$escavacao.'">
                    <div class="percent">'.$escavacao.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Escavação</h3>
            </li>';
        }
        if($contencao) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$contencao.'">
                    <div class="percent">'.$contencao.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Contenção</h3>
            </li>';
        }
        if($fundacao) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$fundacao.'">
                    <div class="percent">'.$fundacao.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Fundação</h3>
            </li>';
        }
        if($superestrutura) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$superestrutura.'">
                    <div class="percent">'.$superestrutura.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Superestrutura</h3>
            </li>';
        }
        if($alvenarias) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$alvenarias.'">
                    <div class="percent">'.$alvenarias.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Alvenarias</h3>
            </li>';
        }
        if($impermeabilizacao) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$impermeabilizacao.'">
                    <div class="percent">'.$impermeabilizacao.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Impermeabilização</h3>
            </li>';
        }
        if($reboco_emboco) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$reboco_emboco.'">
                    <div class="percent">'.$reboco_emboco.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Reboco/Emboço</h3>
            </li>';
        }
        if($contrapiso) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$contrapiso.'">
                    <div class="percent">'.$contrapiso.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Contrapiso</h3>
            </li>';
        }
        if($revestimento_interno) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$revestimento_interno.'">
                    <div class="percent">'.$revestimento_interno.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Revestimento Interno</h3>
            </li>';
        }
        if($rejuntamento_interno) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$rejuntamento_interno.'">
                    <div class="percent">'.$rejuntamento_interno.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Rejuntamento Interno</h3>
            </li>';
        }
        if($divisorias_gesso) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$divisorias_gesso.'">
                    <div class="percent">'.$divisorias_gesso.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Divisórias de Gesso</h3>
            </li>';
        }
        if($forro_gesso) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$forro_gesso.'">
                    <div class="percent">'.$forro_gesso.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Forro de Gesso</h3>
            </li>';
        }
        if($esquadrias_aluminio) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$esquadrias_aluminio.'">
                    <div class="percent">'.$esquadrias_aluminio.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Esquadrias de Aluminio</h3>
            </li>';
        }
        if($guarda_corpo) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$guarda_corpo.'">
                    <div class="percent">'.$guarda_corpo.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Guarda Corpo</h3>
            </li>';
        }
        if($portas_madeira) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$portas_madeira.'">
                    <div class="percent">'.$portas_madeira.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Portas de Madeira</h3>
            </li>';
        }
        if($assentamento_bancadas) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$assentamento_bancadas.'">
                    <div class="percent">'.$assentamento_bancadas.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Assentamento de Bancadas</h3>
            </li>';
        }
        if($assentamento_louças_metais) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$assentamento_louças_metais.'">
                    <div class="percent">'.$assentamento_louças_metais.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Assentamento de Louças e Metais</h3>
            </li>';
        }
        if($pintura_prim_demao) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$pintura_prim_demao.'">
                    <div class="percent">'.$pintura_prim_demao.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Pintura 1ª demão</h3>
            </li>';
        }
        if($pintura_seg_demao) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$pintura_seg_demao.'">
                    <div class="percent">'.$pintura_seg_demao.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Pintura 2ª demão</h3>
            </li>';
        }
        if($limpeza_final) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$limpeza_final.'">
                    <div class="percent">'.$limpeza_final.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Limpeza Final</h3>
            </li>';
        }
        if($fiacao_eletrica) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$fiacao_eletrica.'">
                    <div class="percent">'.$fiacao_eletrica.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Fiação Elétrica</h3>
            </li>';
        }
        if($acabamentos_eletricos) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$acabamentos_eletricos.'">
                    <div class="percent">'.$acabamentos_eletricos.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Acabamentos Elétricos</h3>
            </li>';
        }
        if($ramais_esgoto_aguas_pluviais) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$ramais_esgoto_aguas_pluviais.'">
                    <div class="percent">'.$ramais_esgoto_aguas_pluviais.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Ramais de Esgoto/Águas Pluviais</h3>
            </li>';
        }
        if($ramais_agua_fria_quente) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$ramais_agua_fria_quente.'">
                    <div class="percent">'.$ramais_agua_fria_quente.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Ramais de Água Fria/Água Quente</h3>
            </li>';
        }
        if($ramais_incendio) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$ramais_incendio.'">
                    <div class="percent">'.$ramais_incendio.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Ramais de Incêndio</h3>
            </li>';
        }
        if($instalacoes_gas) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$instalacoes_gas.'">
                    <div class="percent">'.$instalacoes_gas.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Instalações de Gás</h3>
            </li>';
        }
        if($instalacoes_ar_condicionado) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$instalacoes_ar_condicionado.'">
                    <div class="percent">'.$instalacoes_ar_condicionado.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Instalações de Ar Condicionado</h3>
            </li>';
        }
        if($revestimento_fachada) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$revestimento_fachada.'">
                    <div class="percent">'.$revestimento_fachada.'<em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Revestimento Fachada</h3>
            </li>';
        }
        if($limpeza_fachada) {
            echo '<li>
            <div class="status-bar">
                <div class="bar-progress" data-total="'.$limpeza_fachada.'">
                    <div class="percent">'.$limpeza_fachada.'<em>%</em></div>
                </div>
            </div>
            <h3 class="status-tit">Limpeza Fachada</h3>
            </li>';
        }

        echo '</ul>';
    echo '</div>';
endif; ?>