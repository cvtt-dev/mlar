<?php
$settings = get_option('options_gerais');
$group     = $settings['group_vantagens'];

?>
<div class="section ml-bg-purple">
  <div class="background bg-warning"></div>
  <div class="container-4">
    <div class="heading-centered _100">
      <h1 class="body-heading body-heading--branco">O M.LAR JACAREY POSSUI MUITAS VANTAGENS.<br></h1>
      <div class="large-text txt-branco">Veja mais beneficios de morar no M.LAR</div>
    </div>
    <div class="vantagens_slide">



      <?php foreach ($group as $item) : ?>



        <div class="item padded-grid-item">
          <div class="centered-grid-item"><img src="<?php echo wp_get_attachment_url($item['icone']); ?>" alt="" class="icon-large">
            <h6 class="h6-small"><?php echo $item['titulo'] ?></h6>
            <!-- <div class="large-text txt-branco txt-diferenciais">É simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI</div> -->
          </div>
        </div>


      <?php endforeach; ?>
     

    </div>
  </div>
</div>




