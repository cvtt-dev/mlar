<?php get_template_part('templates/html', 'head'); ?>

<body <?php body_class(); ?>>
  <div class="mobile-menu">

    <nav role="navigation" class="mob-nav">
      <div class="wrap-nav">
        <a href="#empreendimento">Empreendimento</a>
        <a href="#form">Contato</a>
        <a href="#sobre">Conheça a M. Lar</a>
        <a href="#faq">Dúvidas</a>
        <a href="#">Portal do Cliente</a>
        <a href="#">Portal do Corretor</a>
      </div>
    </nav>
  </div>
  <div class="ml-main">
    <div class="navigation-container">
      <div class="container-8">
        <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar w-nav">
          <a href="<?php echo get_site_url(); ?>" class="w-nav-brand"><img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-m-lar-empreendimentos.svg" alt="" class="image"></a>
          <nav role="navigation" class="nav-menu-wrapper-2 justify-end w-nav-menu">
            <div class="nav-links nav-links-1">
              <ul class="menu-nav">
             
                <li class="menu-item">
                  <a href="#sobre" class="nav-link-2 w-nav-link">Conheça a M. Lar</a>
                </li>
                <li class="menu-item">
                  <a href="#empreendimento" class="nav-link-2 w-nav-link">Empreendimento</a>
                  <i class="seta-sub"></i>
                  <ul class="menu-nav--submenu">
                    <li class="menu-item--sub">
                      <a href="#galeria" class="nav-link-2 w-nav-link">Galeria</a>
                    </li>
                    <li class="menu-item--sub">
                      <a href="#plantas" class="nav-link-2 w-nav-link">Plantas</a>
                    </li>
                    <li class="menu-item--sub">
                      <a href="#localizacao" class="nav-link-2 w-nav-link">Localização</a>
                    </li>
                    <li class="menu-item--sub">
                      <a href="#ficha-tecnica" class="nav-link-2 w-nav-link">Ficha Técnica</a>
                    </li>
                  </ul>
                </li>
                <li class="menu-item">
                  <a href="#contato" class="nav-link-2 w-nav-link">Contato</a>
                </li>
                <li class="menu-item">
                  <a href="#faq" class="nav-link-2 w-nav-link">Dúvidas</a>
                </li>
               
              <?php session_start(); ?>
              <?php if ($_SESSION['xml']) : ?>
                             
                <li class="menu-item">
                  <a href="<?php echo get_bloginfo('url'); ?>/area-do-cliente" class="nav-link-2 w-nav-link">Portal do Cliente</a>
                </li>
              <?php else : ?>
                <li class="menu-item">
                  <a data-fancybox="login-modal" data-src="#login-modal" href="javascript:;" class="nav-link-2 w-nav-link">Portal do Cliente</a>
                </li>
                <?php endif; ?>

                <li class="menu-item">
                  <a href="#" class="nav-link-2 w-nav-link">Portal do Corretor</a>
                </li>
              </ul>






            </div>
          </nav>
          <div class="sandwich-btn-menu">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </div>
    </div>