<?php
$settings = get_option('options_gerais');
$form     = $settings['form_code'];
?>


<div class="section-7 has-squiggle bg-dark">
    <div id="contato" class="bg-squiggle"></div>
    <div class="main-container-2">
        <div class="container-large-2 align-center">
            <div class="card-body">
                <p class="big-paragraph txt text-form">Queremos te ajudar nessa conquista!</p>
                <div class="form w-form">
                   <?= do_shortcode($form); ?>
                </div>
            </div>

        </div>
        <!-- <a href="#"  class="button-2 ml-button-orange small outline-white center bt w-inline-block">
          <div class="text-block-21 whtas-txt-2 whts-big">Tour Virtual</div>
        </a> -->
    </div>
</div>