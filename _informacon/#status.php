<?php
/* STATUS*/
$statusGeral = get_post_meta( get_the_id(), 'wpcf_status_geral', true );
$statusInstalacoes = get_post_meta( get_the_id(), 'wpcf_status_instalacoes', true );
$statusMovimento = get_post_meta( get_the_id(), 'wpcf_status_movimento', true );
$statusFundacoes = get_post_meta( get_the_id(), 'wpcf_status_fundacoes', true );
$statusEstrutura = get_post_meta( get_the_id(), 'wpcf_status_estrutura_concreto', true );
$statusParedes = get_post_meta( get_the_id(), 'wpcf_status_paredes_paineis', true );
$statusRevestimento = get_post_meta( get_the_id(), 'wpcf_status_revestimento_interno', true );
$statusPavimentacao = get_post_meta( get_the_id(), 'wpcf_status_pavimentacao', true );
$statusEquipamento = get_post_meta( get_the_id(), 'wpcf_status_instalacoes_equipamentos', true );
$statusEsquadrias = get_post_meta( get_the_id(), 'wpcf_status_instalacoes_esquadrias', true );
?>

<?php if ( $statusGeral ) : ?>

<div id="box-status" class="box-bars">
    <h2 class="status-tit-pri">Status geral da obra</h2>

    <div class="status-bar">
        <div class="bar-progress" data-total="<?php echo $statusGeral; ?>">
            <div class="percent"><?php echo $statusGeral; ?><em>%</em></div>
        </div>
    </div>


    <ul class="list-status">

        <?php if ($statusInstalacoes) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusInstalacoes; ?>">
                    <div class="percent"><?php echo $statusInstalacoes; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Instalações Provisórias</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusMovimento) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusMovimento; ?>">
                    <div class="percent"><?php echo $statusMovimento; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Movimento de Terra</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusFundacoes) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusFundacoes; ?>">
                    <div class="percent"><?php echo $statusFundacoes; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Fundações</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusEstrutura) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusEstrutura; ?>">
                    <div class="percent"><?php echo $statusEstrutura; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Estrutura de Concreto</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusParedes) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusParedes; ?>">
                    <div class="percent"><?php echo $statusParedes; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Paredes e Painéis</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusRevestimento) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusRevestimento; ?>">
                    <div class="percent"><?php echo $statusRevestimento; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Revestimento Interno</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusPavimentacao) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusPavimentacao; ?>">
                    <div class="percent"><?php echo $statusPavimentacao; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Pavimentação</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusEquipamento) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusEquipamento; ?>">
                    <div class="percent"><?php echo $statusEquipamento; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Instalações e Equipamentos</h3>
        </li>
        <?php endif; ?>

        <?php if ($statusEsquadrias) : ?>
        <li>
            <div class="status-bar">
                <div class="bar-progress" data-total="<?php echo $statusEsquadrias; ?>">
                    <div class="percent"><?php echo $statusEsquadrias; ?><em>%</em></div>
                </div>
            </div>

            <h3 class="status-tit">Esquadrias</h3>
        </li>
        <?php endif; ?>
    </ul>
</div>
<?php endif; ?>