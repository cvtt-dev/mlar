<?php /* Template Name: Área do Cliente */ ?>
<?php session_start();
$settings = get_option('options_gerais');
if (isset($_GET['sair'])) {
    session_destroy();
    wp_redirect(home_url());
    exit;
}




if ($_SESSION['xml']) :
    //dados
    $xml    = new SimpleXMLElement($_SESSION['xml']);
    $login  = $_SESSION['login'];
    $senha  = $_SESSION['senha'];

    //vars
    $cliente_nome                   = $xml->cliente_nome;
    $cliente_cpf                    = $xml->cliente_cpf;
    $cliente_rg                     = $xml->cliente_rg;
    $cliente_telefone               = $xml->cliente_telefone;
    $cliente_email                  = $xml->cliente_email;
    $cliente_endereco               = $xml->cliente_endereco;
    $cliente_endereco_logradouro    = $xml->cliente_endereco_logradouro;
    $cliente_endereco_numero        = $xml->cliente_endereco_numero;
    $cliente_endereco_compl         = $xml->cliente_endereco_compl;
    $cliente_endereco_pais          = $xml->cliente_endereco_pais;
    $cliente_bairro                 = $xml->cliente_bairro;
    $cliente_cidade                 = $xml->cliente_cidade;
    $cliente_uf                     = $xml->cliente_uf;
    $cliente_cep                    = $xml->cliente_cep;
// echo '<pre>';
// print_r($xml);
// echo '</pre>';
else :
    wp_redirect(get_bloginfo('url'));
endif;
?>
<style>
    .accordion-content.open{
        display: flex;
    }
</style>
<?php get_template_part('tamplates/cliente', 'dados'); ?>
<?php get_template_part('templates/html', 'header'); ?>
<?php get_template_part('templates/cliente', 'banner'); ?>
<div class="main empreendimentos">

    <div class="form-car__wrap" id="editarDados" style="display: none;">
        <h4 class="modal-title" id="EditarInfos">Editar Dados</h4>
        <?php echo do_shortcode('[contact-form-7 title="Restrito: Editar Dados"]'); ?>
    </div>
    <div class="form-car__wrap" id="assistenciaTecnica" style="display: none;">
        <h4 class="modal-title" id="AssistenciaInfos">Assistência Técnica</h4>
        <?php echo do_shortcode('[contact-form-7 title="Restrito: Assistência Técnica"]'); ?>
    </div>


    <div class="menuCliente">
        <div class="container">

            <div class="menuCliente__bar">
                <div class="menuCliente__option">
                    <div class="menuCliente__logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-bar-cli.png" alt="Cliente" class="img-responsive" />
                    </div>
                    <div class=" menuCliente__toppattern">
                        Olá, <?php echo $cliente_nome; ?>
                        <span>CPF:<?php echo $cliente_cpf; ?></span>
                    </div>
                </div>
                <!-- <div class="menuCliente__whatsapp">
                    <a class="menuCliente__link" href="https://api.whatsapp.com/send?phone=5585991326117" target="_blank"> 
                        <i class="icone fa fa-whatsapp"></i>
                        <p class="menuCliente__title">Whatsapp relacionamento<span>99132.6117</span></p>
                    </a>    
                </div> -->

                <div class="menuCliente__option-sair">
                    <ul class="menuCliente__opcoes-login">
                        <!-- <button type="button" >Launch modal</button> -->
                        <li>
                            <a data-fancybox="" data-animation-duration="500" data-src="#editarDados" href="javascript:;" class="editardados modal-alterar" data-effect="mfp-zoom-in">
                                <i class="fa fa-cog"></i> Alterar dados
                            </a>
                        </li>

                        <li>
                            <a class="logout" href="<?= get_permalink(); ?>?sair=true" id="sair_painel_cliente">
                                <i class="fa fa-power-off"></i> Sair
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
    <div class="hidden">
        <input type="hidden" class="cliente_nome" value="<?php echo $cliente_nome; ?>" />
        <input type="hidden" class="cliente_cpf" value="<?php echo $cliente_cpf; ?>" />
        <input type="hidden" class="cliente_rg" value="<?php echo $cliente_rg; ?>" />
        <input type="hidden" class="cliente_telefone" value="(<?php echo substr($cliente_telefone, 4); ?>" />
        <input type="hidden" class="cliente_email" value="<?php echo $cliente_email; ?>" />
        <input type="hidden" class="cliente_endereco" value="<?php echo $cliente_endereco; ?>" />
        <input type="hidden" class="cliente_endereco_logradouro" value="<?php echo $cliente_endereco_logradouro; ?>" />
        <input type="hidden" class="cliente_endereco_numero" value="<?php echo $cliente_endereco_numero; ?>" />
        <input type="hidden" class="cliente_endereco_compl" value="<?php echo $cliente_endereco_compl; ?>" />
        <input type="hidden" class="cliente_endereco_pais" value="<?php echo $cliente_endereco_pais; ?>" />
        <input type="hidden" class="cliente_bairro" value="<?php echo $cliente_bairro; ?>" />
        <input type="hidden" class="cliente_cidade" value="<?php echo $cliente_cidade; ?>" />
        <input type="hidden" class="cliente_uf" value="<?php echo $cliente_uf; ?>" />
        <input type="hidden" class="cliente_cep" value="<?php echo $cliente_cep; ?>" />
    </div>

    <div class="area-informacoes">
        <div class="container">
            <div class="area-informacoes__dash">
                <div class="area-informacoes__content">
                    <h3 class="area-informacoes__titlebg">Contratos</h3>
                    <div class="accordion">
                        <div class="accordion-section">

                            <?php //Contratos
                            $loopC     = 0;
                            $contratos = $xml->listacontratos->contrato_identificador_item;

                            foreach ($contratos as $c) {

                                $codigo             = $c->empreendimento_codigo;
                                $edificacao         = $c->imovel_edificacao;
                                $contrato           = $c->numero_contrato;
                                $data_venda         = $c->contrato_datavenda;
                                $ano_venda          = date('Y', strtotime($data_venda));
                                $status_geral                   = get_post_meta($c, 'wpcf_status_geral', true);
                                // echo $edificacao;

                                //$get_boleto         = 'http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/getcob2avia?CLILOG=' . $login . '&CLISEN=' . $senha . '&EMPREECOD=' . $codigo . '&CONTRATO_NUMERO=' . $contrato . '&MESANOCOB=' . date('m/Y');
                                $get_boleto         = 'http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/getcob2avia?CLILOG=' . $login . '&CLISEN=' . $senha . '&EMPREECOD=' . $codigo . '&CONTRATO_NUMERO=' . $contrato . '&TODOS_BOLETOS=S';


                                $xmlBoleto          = marquiseGetXml($get_boleto);
                                $xmlBoletoObj       = new SimpleXMLElement($xmlBoleto);


                                $url_boleto = "";
                                foreach ($xmlBoletoObj->listacobrancas->cobranca_item as $b) {
                                    $url_link     = 'http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/geraboleto?AGEFIN=' . $b->agefin_id . '&COBNUM=' . $b->cobranca_numerodocumento . '&DATA_REF=' . $b->cobranca_vencimento . '&FMT=PDF';
                                    $url_boleto   .= '<li><a target="_blank" href="' . $url_link . '">' . $b->cobranca_vencimento . ': R$ ' . $b->cobranca_valor . '</a></li>';
                                }

                                $url_planilha   = 'http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/geraplanilhapdf?CLILOG=' . $login . '&CLISEN=' . $senha . '&EMPREECOD=' . $codigo . '&CONTRATO_NUMERO=' . $contrato . '&DATA_REF=' . date('d/m/Y');
                                $url_imposto    = 'http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/gerairpdf?CLILOG=' . $login . '&CLISEN=' . $senha . '&EMPREECOD=' . $codigo . '&CONTRATO_NUMERO=' . $contrato . '&DATA_REF=31/12/' . date("Y", strtotime("-1 year"));                                
                                $url_impostos = "";

                                while ($ano_venda <= '2016') {
                                    $url_ir = 'http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/gerairpdf?CLILOG=' . $login . '&CLISEN=' . $senha . '&EMPREECOD=' . $codigo . '&CONTRATO_NUMERO=' . $contrato . '&DATA_REF=31/12/' . $ano_venda;
                                    $url_impostos .= '<li><a target="_blank" href="' . $url_ir . '">Ano Base ' . $ano_venda . '</a></li>';
                                    $ano_venda++;
                                }


                                echo '<button data-arrow="#contrato' . $loopC . '"class="accordion-title area-informacoes__contrato-title ' . ($loopC == 0 ? "active" : "") . '" id="dropdown-info" ><i class="area-informacoes__icone"></i>' . $c->empreendimento_nome . '</button>';
                                echo '<div id="contrato' . $loopC . '" class="accordion-content area-informacoes__contrato-content ' . ($loopC == 0 ? "open" : "") . '">';

                                echo '<div class="area-informacoes__panel-body">';
                                echo '<ul class="area-informacoes__nav-pills" id="painel-opcoes">
                                                    <li>
                                                    <a data-fancybox="" data-animation-duration="500" data-src="#assistenciaTecnica" href="javascript:;" class="modal-assistencia area-informacoes__btn">
                                                     
                                                            <i class="fa fa-cog" aria-hidden="true"></i> Assistência Técnica
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="' . $url_planilha . '" class="planilha area-informacoes__btn btn-default" target="_blank">
                                                            <i class="fa fa-file-text-o" aria-hidden="true"></i> Extrato Financeiro
                                                        </a>
                                                    </li>';
                                echo '<li>
                                                        <a href="#" class="area-informacoes__btn" id="drop3">
                                                            <i class="fa fa-file-text-o" aria-hidden="true"></i> Demonstrativo IR
                                                        </a>
                                                        <ul class="area-informacoes__dropdown-menu" id="ir-menu" aria-labelledby="drop3">
                                                            <form class="area-informacoes__ir-form-action" action="' . get_template_directory_uri() . '/_informacon/imposto-renda.php" method="GET" target="_blank">
                                                            <input name="url" type="hidden" value="http://integracao.marquise.com.br:7070/cgi/wminformaconcgi.exe/gerairpdf?CLILOG=' . $login . '&CLISEN=' . $senha . '&EMPREECOD=' . $codigo . '&CONTRATO_NUMERO=' . $contrato . '&DATA_REF=31/12/"/>
                                                                <div class="form-group area-informacoes__ir-form-group">
                                                                <input name="ano" type="text" class="area-informacoes__ir-form" placeholder="Ano Base" required>
                                                                </div>
                                                                <button type="submit" class="area-informacoes__ir-btn">Ir</button>
                                                            </form>
                                                        </ul>
                                                    </li>';
                                echo ($url_boleto ? '
                                                    <li>
                                                        <a href="#" class="boleto area-informacoes__btn " id="drop4">
                                                            <i class="fa fa-print" aria-hidden="true"></i> 2ª via de boleto
                                                        </a>
                                                        <ul class="boleto_dropdown" id="boleto" aria-labelledby="drop4" style="display: none;">
                                                            ' . $url_boleto . '
                                                        </ul>
                                                    </li>' : '<li><a class="boleto area-informacoes__btn area-informacoes__disabled" href="#" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> 2ª via de Boleto</a></li>');
                                echo '</ul>';

                                echo '<div class="area-informacoes__wrap-small">';
                                echo '<span class="small"><b>*</b> O extrato será atualizado com o prazo de 72h após o pagamento da parcela, para que seja efetuada a baixa do pagamento.</span>';
                                // echo '<span class="small"><b>**</b> São disponibilizados os boletos somente no dentro do mesmo mês de vencimento. As solicitações de boletos fora do mês do vencimento, deverão ser feitas ao setor de Relacionamento com Cliente da Marquise, para que sejam incluídas as devidas correções.</span>';
                                echo '<span class="small"><b>**</b> Os boletos serão disponibilizados de acordo com seu vencimento. Para pagamentos em atraso, serão aplicadas as correções contratuais. Caso o boleto seja pago após o mês vigente de seu vencimento,  o resíduo referente a correção do índice será aplicado na próxima parcela.</span>';
                                echo '</div>';

                                //CAMPOS HIDDEN
                                echo '<div class="area-informacoes__wrap-small">'; // inicio wrap
                                echo '<div class="hidden">
                                                    <input type="hidden" class="' . $contrato . '_imovel_nome" value="' . $c->empreendimento_nome . '"/>
                                                    <input type="hidden" class="' . $contrato . '_imovel_codigo" value="' . $codigo . '" />
                                                    <input type="hidden" class="' . $contrato . '_imovel_unidade" value="' . $c->imovel_unidade . '"/>
                                                    <input type="hidden" class="' . $contrato . '_imovel_endereco_emp" value="' . $c->empreendimento_endereco->logradouro . ' - ' . $c->empreendimento_endereco->bairro . '"/>
                                                    <input type="hidden" class="' . $contrato . '_imovel_contrato" value="' . $contrato . '"/>
                                                </div>';
                                echo '</div>'; // fim wrap-small


                                echo '<div class="area-informacoes__wrap-small">'; // inicio wrap

                                echo '<h5 class="title-detalhes">Detalhes do Empreendimento</h5>';

                                echo '<div class="infos">';
                                echo '<p><b>Unidade:</b> ' . $c->imovel_unidade . '</p>';
                                echo '<p><b>Código:</b> ' . $codigo . '</p>';
                                echo '<p><b>Endereço:</b> ' . $c->empreendimento_endereco->logradouro . ' - ' . $c->empreendimento_endereco->bairro . ' - ' . $c->empreendimento_endereco->cidade . ' - ' . $c->empreendimento_endereco->uf . ' - ' .
                                    $c->empreendimento_endereco->cep . '</p>';
                                echo '</div>';

                                echo '</div>'; // fim wrap-small

                                echo '<div class="area-informacoes__wrap-small">'; // inicio wrap

                                echo '<h5 class="title-detalhes">Dados do Contrato</h5>';

                                echo '<div class="infos">';
                                echo '<p><b>Nº do Contrato:</b> ' . $contrato . '</p>';
                                echo '<p><b>Data de Venda:</b> '  . $c->contrato_datavenda . '</p>';
                                echo '<p><b>Valor da Venda:</b> ' . $c->contrato_valordavenda . '</p>';
                                echo '</div>';

                                echo '</div>'; // fim wrap-small

                                // $c->contrato_database
                                // if($edificacao != 'Vagas de Garagem'):

                                $args = array(
                                    'post_type'     => 'mar_empreendimentos',
                                    'meta_query' => array(
                                        array(
                                            'key' => 'wpcf_cod_empreendimento',
                                            'value' => like_escape($c->empreendimento_codigo),
                                            'compare' => 'EQUAL'
                                        )
                                    ),
                                    'showposts'     => 1,
                                );

                                $empreendimento = get_posts($args);
                                /* STATUS*/

                                $status_geral  = get_post_meta($empreendimento[0]->ID, 'wpcf_status_geral', true);

                                echo  $status_geral ? ' <div class="area-informacoes__wrap-small">
                                <h3 class="title-manual">Andamento das obras</h3>
                                 <div class="area-informacoes__btns-wrap">
                                <a href="#statusObra_' . $loopC . '" class="modal-assistencia" target="_blank">Ver Status da Obra</a></div></div>' : '';


                                // get_template_part('_informacon/status');
                                // endif;
                                include(locate_template('_informacon/documentos.php'));
                            ?>

                                <div class="mfp-modal mfp-modal--formAssistencia mfp-hide" id="statusObra_<?= $loopC; ?>">
                                    <h4 class="modal-title" id="AssistenciaInfos">Assistência Técnica</h4>
                                    <?php include(locate_template('_informacon/status.php')); ?>
                                </div>
                            <?php
                                echo '</div>';
                                echo '</div>';
                                $loopC++;
                            }
                            ?>


                            <div class="accordion-title--form area-informacoes__contrato-title"><i class="area-informacoes__icone"></i>Relacionamento cliente</div>
                            <div class="accordion-content area-informacoes__contrato-content open" style="display:block;">
                                <!-- <header class="accordion-header mq-header-geo">
                                    <h2 class="title">Entre em contato</h2>
                                    <h3 class="desc">aqui você pode entrar em contato conosco em relação ao seu empreendimento Marquise.</h3>
                                </header> -->
                                <div class="mq-clientes__form">
                                    <?= do_shortcode('[contact-form-7 title="Restrito: Fale Conosco"]'); ?>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>

                <div class="area-informacoes__sidebar">

                    <h3 class="area-informacoes__titlebg">Dados do Cliente</h3>

                    <div class="">
                        <ul class="listauser">
                            <li class="userinfo"><i class="fa fa-user" aria-hidden="true"></i></i>Dados Pessoais</li>
                            <li class="userinfo userinfo--sub">
                                <?php echo $cliente_nome; ?></br>
                                CPF: <?php echo $cliente_cpf; ?></br>
                                RG: <?php echo $cliente_rg; ?>
                            </li>
                            <li class="userinfo"><i class="fa fa-phone" aria-hidden="true"></i>Telefone</li>
                            <li class="userinfo userinfo--sub">(<?php echo substr($cliente_telefone, 4); ?></li>
                            <li class="userinfo"><i class="fa fa-envelope" aria-hidden="true"></i>Email</li>
                            <li class="userinfo userinfo--sub"><?php echo $cliente_email; ?></li>
                            <li class="userinfo"><i class="fa fa-map-marker" aria-hidden="true"></i>Endereço</li>
                            <li class="userinfo userinfo--sub">
                                <?php echo $cliente_endereco; ?></br>
                                <?php echo $cliente_bairro; ?> - CEP: <?php echo $cliente_cep; ?></br>
                                <?php echo $cliente_cidade; ?> - <?php echo $cliente_uf; ?> - <?php echo $cliente_endereco_pais; ?></br>
                            </li>
                        </ul>
                    </div>

                    <h3 class="area-informacoes__titleSmall">Documentos Importantes</h3>
                    <div class="area-informacoes__btns">
                        <?php
                        $financiamento  = get_post_meta(get_the_ID(), 'planilhas_financiamento', true);
                        $cessao         = get_post_meta(get_the_ID(), 'planilhas_cessao', true);


                        $btn_tit         = get_post_meta($empreendimento[0]->ID, 'btn_ext_ac_tit', true);
                        $btn_link         = get_post_meta($empreendimento[0]->ID, 'btn_ext_ac_ulr', true);
                        $btn_target        = get_post_meta($empreendimento[0]->ID, 'btn_ext_ac_target', true);
                        $btn_tipo       = get_post_meta($empreendimento[0]->ID, 'btn_ext_ac_tipo', true);


                        //$btnAndamento   = get_post_meta(get_the_ID(), 'emp_and_obra', true);

                        echo ($financiamento ? '<a href="' . wp_get_attachment_url($financiamento) . '" download="" target="_blank" class="area-informacoes__link">Cartilha de Financiamento</a>' : '');
                        echo ($cessao ? '<a href="' . wp_get_attachment_url($cessao) . '" download="" target="_blank" class="area-informacoes__link">Cessão de Direitos</a>' : '');
                        echo    $btn_link && $btn_tit ? '<a href="' . $btn_link . '" class="area-informacoes__link" target="' . $btn_target . '" ' . $btn_tipo . '>' . $btn_tit . '</a>' : '';

                        ?>
                    </div>

                </div>


            </div>
        </div>
    </div>






</div>
<style>
    .invistacom_box_status.list-status li {
        display: flex;
        flex-direction: column-reverse;
    }

    .invistacom_box_status.list-status li .status-bar {
        background-color: #e6e6e6;
        position: relative;
        display: flex;
        width: 100%;
        height: 20px;
    }

    .invistacom_box_status.list-status li .status-bar.status-bar--geral {
        height: 35px;
    }



    .invistacom_box_status.list-status li .status-bar .bar-progress {
        position: absolute;
        top: 0;
        left: 0;
        background: #f26522;
        height: 100%;
    }

    .invistacom_box_status.list-status li .status-bar .bar-progress .percent {
        color: #fff;
        font-family: 'Montserrat';
        line-height: 1;
        display: flex;
        justify-content: center;
        height: 20px;
        align-items: center;
    }

    .invistacom_box_status.list-status li .status-bar.status-bar--geral .bar-progress .percent {
        height: 35px;
    }

    .invistacom_box_status h3.status-tit {
        font-size: 15px;
        margin-bottom: 3px;
        margin-top: 10px;
        color: #4c4c4c;
    }

    .invistacom_box_status h3.status-tit.status-tit--geral {
        font-weight: 600 !important;
        font-size: 20px;
    }
</style>


<?php

$words = explode(" ", $cliente_nome);
$cliente_nome_firstL = array();
foreach ($words as $word) {
    if (ctype_upper($word)) $cliente_nome_firstL[] = $word[0] . strtolower(substr($word, 1));
    else $cliente_nome_firstL[] = $word;
}
$cliente_nome_firstL = implode($cliente_nome_firstL, " ");

?>
<script>
    if (<?= $loopC++; ?> == 1) {
        document.getElementById('cli_nome').value = "<?= $cliente_nome_firstL  ?>";
        document.getElementById('cli_email').value = "<?= $cliente_email ?>";
        document.getElementById('cli_telefone').value = "<?= substr($cliente_telefone, 4)  ?>";
        document.getElementById('cli_emp').value = "<?= $contratos[0]->empreendimento_nome ?>";
    }
</script>

<?php get_template_part('templates/scroll', 'bar'); ?>
<?php get_template_part('templates/html', 'footer'); ?>