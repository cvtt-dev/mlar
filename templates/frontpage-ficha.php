<?php
$settings = get_option('options_gerais');
$fichas     = $settings['group_ficha'];
?>

<div id="ficha-tecnica" class="section bg-gray-4 fichatecnica ">
  <div class="container-9">
    <div class="row row-split-content ficha">
      <div class="content-width-small">
        <h1 class="body-heading-2 titulowhite titulo-3 titulo-center"><span class="text-span-9">ficha técnica</span><br></h1>
        <ul role="list" class="list-grid margin-top lista-ficha w-list-unstyled">

          <?php foreach ($fichas as $ficha) : ?>


            <li class="icon-grid-list-item">
              <div class="circle-small"><img src="https://uploads-ssl.webflow.com/5f5a3c1606e4e2798a5eed6f/5f5a3c177f73051bdecb9099_icon-check-small.svg" alt=""></div>
              <div class="text-space-left-large-2">
                <div class="txt-lista"><strong><?= $ficha['titulo'] ?><?= $ficha['desc']? ':': ''; ?><br></strong></div>
                <div class="text-block-37 <?= $ficha['desc']? '': 'fichatecnica__d-none'; ?>"><?= $ficha['desc'] ?><br></div>
              </div>
            </li>


          <?php endforeach; ?>



        </ul>
      
      
      </div>
    </div>
    <!-- <div class="div-block-4 mb">
      <a href="#form" class="button-2 small outline-white center bt w-inline-block">
        <div class="text-block-21 whtas-txt-2 whts-big">Viva o bem estar</div>
      </a>
    </div> -->
  </div>
</div>