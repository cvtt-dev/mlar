<div id="mascote-emi" class="emi show upper">
</div>
<div class="mlar-scrollbar open" id="scrollbar">
    <div class="container">
        <div class="scrllbr-wrap">
            <a class="item" href="tel:558532233223">
                <i class="mlar-icon mlar-icon--phone"></i>
                <div class="wrap-text">
                    <h3 class="title">Ligue agora</h3>
                    <span class="info">85 3223.3223</span>
                </div>
            </a>

            <a class="item" href="#" target="_blank">
                <i class="mlar-icon mlar-icon--chat"></i>
                <div class="wrap-text">
                    <h3 class="title">Atendimento</h3>
                    <span class="info">Online</span>
                </div>
            </a>
            <a class="item" href="https://wa.me/5585994070060?text=Olá, vi vocês no site e gostaria de saber mais sobre." target="_blank">
                <i class="mlar-icon mlar-icon--wpp"></i>
                <div class="wrap-text">
                    <h3 class="title">Whatsapp</h3>
                    <span class="info">(85) 99407-0060</span>
                </div>
            </a>

        </div>
    </div>
</div>