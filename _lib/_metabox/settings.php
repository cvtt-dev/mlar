<?php
add_filter('mb_settings_pages', 'prefix_options_page');
function prefix_options_page($settings_pages)
{
    $settings_pages[] = array(
        'id'          => 'theme-options',
        'option_name' => 'options_gerais',
        'menu_title'  => __('Opções do Tema', 'textdomain'),
        // 'parent'      => 'themes.php',
    );
    return $settings_pages;
}

add_filter('rwmb_meta_boxes', 'prefix_options_meta_boxes');


function prefix_options_meta_boxes($meta_boxes)
{
    $meta_boxes[] = array(
        'id'             => 'settings_vantagem',
        'title'          => 'Vantagens MLar',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(


            array (
				'id' => 'group_vantagens',
				'type' => 'group',
				'name' => '',
				'fields' => array(
					array (
						'id' => 'titulo',
						'type' => 'text',
						'name' => 'Titulo',
					),
					array (
						'id' => 'icone',
						'type' => 'single_image',
						'name' => 'Icone',
					),
				),
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'expanded',
			),
        ),


    );
    $meta_boxes[] = array(
        'id'             => 'settings_ficha',
        'title'          => 'Ficha Técnica MLar',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(


            array (
				'id' => 'group_ficha',
				'type' => 'group',
				'name' => '',
				'fields' => array(
					array (
						'id' => 'titulo',
						'type' => 'text',
						'name' => 'Titulo',
					),
					array (
						'id' => 'desc',
						'type' => 'textarea',
						'name' => 'Descrição',
					),
					
				),
				'clone' => 1,
				'sort_clone' => 1,
				'default_state' => 'expanded',
			),
        ),


    );


    $meta_boxes[] = array(
        'id'             => 'settings_galeria',
        'title'          => 'Galeria MLar',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(


            array(
                'id' => 'galeria_images',
                'type' => 'image_advanced',
                'name' => '',
                'max_status' => false,
            ),
        ),


    );



    $meta_boxes[] = array(
        'id'             => 'settings_plantas',
        'title'          => 'Plantas MLar',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(


            array(
                'id' => 'plantas_images',
                'type' => 'image_advanced',
                'name' => '',
                'max_status' => false,
            ),
        ),


    );


    $meta_boxes[] = array(
        'id'             => 'settings_empresas',
        'title'          => 'Empresas Marquise',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(


            array(
                'id' => 'empresas_images',
                'type' => 'image_advanced',
                'name' => '',
                'max_status' => false,
            ),
        ),


    );


    $meta_boxes[] = array(
        'id'             => 'settings_catalogo',
        'title'          => 'Informações gerais',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(

            array(
                'type' => 'heading',
                'name' => 'Mídias Sociais',
            ),

            array(
                'name' => 'URL Facebook',
                'id'   => 'settings_facebook_url',
                'type' => 'url',
            ),

            array(
                'name' => 'URL Instagram',
                'id'   => 'settings_instagram_url',
                'type' => 'url',
            ),

            array(
                'name' => 'URL Youtube',
                'id'   => 'settings_youtube_url',
                'type' => 'url',
            ),

            array(
                'type' => 'heading',
                'name' => 'Outras informações',
            ),

            array(
                'name' => 'WhatsApp',
                'id'   => 'settings_whatsapp',
                'type' => 'text',
            ),
            array('type' => 'divider',),
            array(
                'name' => 'Endereços',
                'id' => 'group_ends',
                'type' => 'group',
                'clone'       => true,
                'sort_clone'  => true,
                'collapsible' => true,
                'group_title' => array('field' => 'end_titulo'), // ID of the subfield
                'save_state' => true, 'clone' => true,
                'sort' => true,

                'fields' => array(
                    array(
                        'name' => 'Título',
                        'id' => 'end_titulo',
                        'type' => 'text',
                        'columns' => 12,
                    ),

                    array(
                        'name' => 'Logradouro/Rua/Av',
                        'id' => 'end_logradouro',
                        'type' => 'text',
                        'columns' => 4,
                    ),

                    array(
                        'name' => 'Estado',
                        'id' => 'end_estado',
                        'type' => 'text',
                        'columns' => 4,
                    ),

                    array(
                        'name' => 'Cidade',
                        'id' => 'end_cidade',
                        'type' => 'text',
                        'columns' => 4,
                    ),

                    array(
                        'name' => 'Bairro',
                        'id' => 'end_bairro',
                        'type' => 'text',
                        'columns' => 4,
                    ),

                    array(
                        'name' => 'CEP',
                        'id' => 'end_cep',
                        'type' => 'text',
                        'columns' => 4,
                    ),

                    array(
                        'name' => 'Telefone',
                        'id' => 'end_telefone',
                        'type' => 'text',
                        'columns' => 4,
                    ),

                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'             => 'settings_form',
        'title'          => 'Codigo Formulário',
        'context'        => 'normal',
        'settings_pages' => 'theme-options',
        'fields'         => array(


            array(
                'id' => 'form_code',
                'type' => 'text',
                'name' => '',
                'desc' => 'cole aqui o shortcode do formulário de contato.',
              
            ),
        ),


    );


    return $meta_boxes;
}
