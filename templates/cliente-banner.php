<?php
    $img = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

<section class="mq-banner mq-banner--medium" style="background-image: url('<?php echo $img; ?>'); ">
    <div class="container">

        <div class="mq-banner__content">
            <h3 class="title title--medium"><?php the_title(); ?></h3>
        </div>

    </div>
</section>