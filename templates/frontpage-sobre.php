<div id="sobre" class="section bg-gray-4 novoform">
  <div class="container-9">
    <div class="row row-split-content split-small">
      <div class="content-width-small">
        
        <h1 class="body-heading-2 titulowhite titulo-3"><span class="text-span-9">FELICIDADE É CHAMAR SUA
CASA DE MEU LAR.</span><br></h1>
        <div class="div-block">
          <div class="content-width-medium-2">
			  <div class="large-text">Chegamos para tornar real o lar que você sempre sonhou. <br>Com M de Morar bem, como a vida deve ser.</br>
M de moderna, para continuar evoluindo com você</br>
M de muitas maneiras para ser feliz.</br>
M de Marquise.</br>
Queremos fazer parte da sua maior realização.</br>
Nosso empreendimento é pensado para conectar tradições, experiências e pessoas.
Tudo isso em um lugar que te encante diariamente.</div>
            <a href="https://www.youtube.com/watch?v=Dkc7BqTnyzY" target="_blank" class="button-2 small outline-white center bt w-inline-block">
              <div class="text-block-21 whtas-txt-2 whts-big">Confira nosso vídeo</div>
            </a>
          </div>
        </div>
      </div>
      <div class="content-width-medium vid-thumb-cover">
        <a class="play-btn" data-fancybox href="https://www.youtube.com/watch?v=gO2qvkw-5xU"></a>
        <div class="card no-border shadow-small"><img src="<?php echo get_template_directory_uri() ?>/uploads/images/img_video_popup_vazado.png" loading="lazy" sizes="(max-width: 479px) 95vw, (max-width: 767px) 66vw, (max-width: 991px) 67vw, (max-width: 1919px) 53vw, 620.5625px" srcset="<?php echo get_template_directory_uri() ?>/uploads/images/img_video_popup_vazado.png 500w, <?php echo get_template_directory_uri() ?>/uploads/images/img_video_popup_vazado.png 800w, <?php echo get_template_directory_uri() ?>/uploads/images/img_video_popup_vazado.png 1080w, <?php echo get_template_directory_uri() ?>/uploads/images/img_video_popup_vazado.png 1130w" alt="" class="image-119"></div>
      </div>
    </div>
  </div>
</div>