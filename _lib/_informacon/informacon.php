<?php
//GET INFORMACON INFOS
function GetDadosXml($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    $xmlstr = curl_exec($ch);
    curl_close($ch);
    return $xmlstr;
}

function informacon_get_user() {
    session_start();
    $login  = $_POST['login'];
    $senha  = $_POST['senha'];
    $path   = "http://servico.wrengenharia.com.br:85";
    $url    = $path."/cgi/wminformaconcgi.exe/validausuario?CLILOG=".$login."&CLISEN=".$senha;

    if(!$login || !$senha) {
        $status = 2;
    }

    else {
        $xmlstr = GetDadosXml($url);
        $xmlobj = new SimpleXMLElement($xmlstr);
        if($xmlobj->validacaoresultado == 'OK' && $xmlobj->listacontratos->contrato_identificador_item->empreendimento_codigo == "JAC.01.0001") {
            $_SESSION['login'] = $login;
            $_SESSION['senha'] = $senha;
            $_SESSION['xml']   = $xmlobj->asXML();
            $status = 1;
        } else {
            $status = 2;
        }
    }

    echo json_encode($status);
    die();
}

add_action('wp_ajax_getUser', 'informacon_get_user');
add_action('wp_ajax_nopriv_getUser', 'informacon_get_user');

function informacon_get_pass() {
    session_start();

    $senha  = $_POST['senha'];
    $path   = "http://servico.wrengenharia.com.br:85";
    $url    = $path."/cgi/wminformaconcgi.exe/alterasenhausuario?CLILOG=".$_SESSION['login']."&CLISEN=".$_SESSION['senha']."&CLINOVASEN=".$senha;
    // print_r($url);

    if(!$senha){
        $status = 2;
    } else {

        $xmlstr = GetDadosXml($url);
        $xmlobj = new SimpleXMLElement($xmlstr);

        if($xmlobj->validacaoresultado == 'OK' && $xmlobj->listacontratos->contrato_identificador_item->empreendimento_codigo == "JAC.01.0001") {
            $_SESSION['senha'] = $senha;
            $status = 1;
        } else {
            if($xmlobj->tipodeerro == 'CLIENTE INEXISTENTE'){
                $status = 3;
            }
            if($xmlobj->tipodeerro == 'SENHA INCORRETA'){
                $status = 4;
            } else {
                $status = 2;
            }
        }
    }

    echo json_encode($status);
    die();
}

add_action('wp_ajax_getPass', 'informacon_get_pass');
add_action('wp_ajax_nopriv_getPass', 'informacon_get_pass');