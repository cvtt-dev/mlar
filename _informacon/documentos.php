<?php
$code = $c->empreendimento_codigo;

$args = array(
    'post_type'     => 'mar_empreendimentos',
    'meta_query' => array(
        array(
            'key' => 'wpcf_cod_empreendimento',
            'value' => like_escape($code),
            'compare' => 'EQUAL'
        )
    ),
    'showposts'     => 1,
);

$empreendimento = get_posts($args);

$planilha       = get_post_meta( $empreendimento[0]->ID, 'emp_planilha', true);
$planilha_sind  = get_post_meta( $empreendimento[0]->ID, 'emp_planilha_sind', true);
$link_planta    = get_post_meta( $empreendimento[0]->ID, 'url_plantas', true);
// echo '<pre>';
// var_dump($link_planta);
// echo '</pre>';

// OBS: ESSAS OPÇÕES SÓ SERÃO HABILITADAS NO PAINEL SE O CÓDIGO DO EMPREENDIMENTO ESTIVER CADASTRADO.
?>


<?php if($link_planta) : ?>
<div class="area-informacoes__wrap-small">
    <h3 class="title-manual">Plantas</h3>
    <div class="area-informacoes__btns-wrap">
        <?php if($link_planta): ?>
            <a download href="<?php echo $link_planta; ?>" target="_blank">Baixar Plantas</a>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>

<?php if($planilha || $planilha_sind): ?>
<div class="area-informacoes__wrap-small">
    <h3 class="title-manual">Manuais do Empreendimento</h3>
    <div class="area-informacoes__btns-wrap">
        <?php if($planilha): ?>
          <a href="<?php echo wp_get_attachment_url($planilha); ?>" download="Manual Proprietário - <?php echo get_the_title($empreendimento[0]->ID); ?>" target="_blank">Manual do Proprietário</a>
        <?php endif; ?>
        <?php if($planilha_sind): ?>
            <a href="<?php echo wp_get_attachment_url($planilha_sind); ?>'" download="Manual Síndico - <?php echo get_the_title($empreendimento[0]->ID); ?>" target="_blank">Manual do Síndico</a>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
