<?php 
if($_GET['action'] == 'logout') {
    session_start();
    session_destroy();
    //$_SESSION = '';
    wp_redirect( home_url() ); exit;
}else{
    session_start();
    if ($_SESSION['cliente']) {
    	$user_dados = $_SESSION['cliente'];
    }elseif($_SESSION['corretor']){
    	$user_dados = $_SESSION['corretor'];
    }
    $indexUsuarios = new Index_Usuarios();
    if ($user_dados) {
    	if ($user_dados->tipo == 0) {
        	$cliente = $indexUsuarios->getCliente('cpf', $user_dados->cpf);
    	}elseif($user_dados->tipo == 1){
        	$corretor = $indexUsuarios->getCorretor('creci', $user_dados->creci);
    	}
    ?>
    <div class="main">
        <div class="loggedbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                    Olá, <?php echo $user_dados->nome ?>  |  <?php if($user_dados->tipo == 0){ echo "Cliente"; }else{ echo 'Corretor'; }  ?>  | <a class="user_logout" href="?action=logout">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } 
} ?>